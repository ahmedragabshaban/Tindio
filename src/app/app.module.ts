import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { IonicStorageModule } from '@ionic/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDvKBWXCKVdoPQvV0fRpwTMNWn5hTBXqm0",
    authDomain: "tindio-b88e7.firebaseapp.com",
    databaseURL: "https://tindio-b88e7.firebaseio.com",
    projectId: "tindio-b88e7",
    storageBucket: "tindio-b88e7.appspot.com",
    messagingSenderId: "910553072415",
    appId: "1:910553072415:web:96c64150d7887b10c2a62a",
    measurementId: "G-3002KJND7W"
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,HttpClientModule,IonicModule.forRoot(), AppRoutingModule,    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule
],
  providers: [   GooglePlus,Facebook,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

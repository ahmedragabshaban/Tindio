import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { UserService } from './user.service';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';


const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // user: Observable<firebase.User>;
  platform: any;
  auth: any = {};
  _uid: string = null;
  authenticationState = new BehaviorSubject(false);

  constructor(private afAuth: AngularFireAuth, private storage: Storage,
    private gplus: GooglePlus, private facebook: Facebook,
    public user: UserService, private Platform: Platform
  ) {

    this.Platform.ready().then(() => {
      this.checkToken();
    });

  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    })
  }

  get basic() {
    return this.user.basic;
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }
 
  isAuthenticated() {
    return this.authenticationState.value;
  }
  
  loginGoogle() {
    return new Promise((resolve, reject) => {
      if (this.basic.google) {
        firebase.auth().currentUser.unlink('google.com').then(() => {
          this.user.ref.doc(this.basic.uid).update({ google: null }).then(() => {
            this.user.setUserGlobalVariable(this.basic.uid).then(() => {
              resolve('Google has been unlinked successfully');
            });
          });
        }).catch(err => {
          reject(err);
        });
      } else {
        var ggcred = null;
        let clientid = (this.platform == 'android') ? '910553072415-qivp5uc7a3j1fcrilm7h8fb8bi9h3cnp.apps.googleusercontent.com' : '910553072415-qivp5uc7a3j1fcrilm7h8fb8bi9h3cnp.apps.googleusercontent.com';
        this.gplus.login({
          'webClientId': clientid,
          'offline': true
        }).then((ggres: any) => {
          // console.log(ggres);
          let credential = firebase.auth.GoogleAuthProvider.credential(ggres.idToken);
          firebase.auth().signInAndRetrieveDataWithCredential(credential).then((res: any) => {
            res = res.user;
            console.log(res);
            this.user.ref.doc(res.uid).get().then((q) => {
              //testing                 
              this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                this.authenticationState.next(true);
              });
              if (!q.exists) {
                let user: any = this.genSocialUser(res);
                user.google = ggres;
                // this.createUser(res.uid, user).then(() => {
                  // this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                  //   this.authenticationState.next(true);
                  // });
                  resolve('Successfully connected with Google');
                // });
              } else {
                this.user.ref.doc(res.uid).update({ google: ggres }).then(() => {
                  this.user.setUserGlobalVariable(res.uid).then(() => {
                    this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                      this.authenticationState.next(true);
                    });
                    resolve('Successfully connected with Google');
                  });
                });
              }
            });

          }).catch(err => {
            switch (err.code) {
              case 'auth/account-exists-with-different-credential':
                ggcred = err.credential;
                this.fetchProvidersForEmail(err.email).then((methods) => {
                  if (methods[0] === 'facebook.com') {
                    if (this.platform == 'ios') {
                      let _err = err;
                      _err.method = 'facebook.com';
                      _err.message = 'Email ' + err.email + ' has been used with Facebook login. Do you want to continue with Facebook login?';
                      reject(_err);
                    } else {
                      var provider = new firebase.auth.FacebookAuthProvider();
                      provider.setCustomParameters({ 'login_hint': err.email });
                      firebase.auth().signInWithRedirect(provider).then(() => {
                        firebase.auth().currentUser.linkAndRetrieveDataWithCredential(ggcred);
                      }).then((res: any) => {
                        this.user.setUserGlobalVariable(res.uid).then(() => {
                          this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                            this.authenticationState.next(true);
                          });
                          resolve(res);
                        });
                      }).catch((err) => {
                        reject(err);
                      });
                    }
                  } else {
                    let _err = err;
                    _err.method = 'password';
                    _err.message = 'Email ' + err.email + ' has been used, do you want to continue login with this email?';
                    reject(_err);
                  }
                }).catch((err) => {
                  reject(err);
                });
                break;
              default:
                reject(err);
                break;
            }
          });

        }).catch(err => {
          reject(err);
        });
      }
    });
  }


  loginFb() {
    return new Promise((resolve, reject) => {
      if (this.basic.facebook) {
        firebase.auth().currentUser.unlink('facebook.com').then(() => {
          this.user.ref.doc(this.basic.uid).update({ facebook: null }).then(() => {
            this.user.setUserGlobalVariable(this.basic.uid).then(() => {
              resolve('Facebook has been unlinked successfully');
            });
          });
        }).catch(err => {
          reject(err);
        });
      } else {
        var fbcred = null;
        this.facebook.login(['public_profile', 'email']).then((fbres) => {
          console.log(fbres);
          let credential = firebase.auth.FacebookAuthProvider.credential(fbres.authResponse.accessToken);
          firebase.auth().signInAndRetrieveDataWithCredential(credential).then((res: any) => {
            res = res.user;
            this.user.ref.doc(res.uid).get().then((q) => {
              this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                this.authenticationState.next(true);
              });
              if (!q.exists) {
                let user: any = this.genSocialUser(res);
                user.facebook = fbres;
                // this.createUser(res.uid, user).then(() => {
                  //set user token
                  // this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                  //   this.authenticationState.next(true);
                  // });
                  resolve('Successfully connected with Facebook');
                // });
              } else {
                this.user.ref.doc(res.uid).update({ facebook: fbres }).then(() => {
                  this.user.setUserGlobalVariable(res.uid).then(() => {
                    //set user token
                    this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                      this.authenticationState.next(true);
                    });
                    resolve('Successfully connected with Facebook');
                  });
                })
              }
            });
          }).catch(err => {
            switch (err.code) {
              case 'auth/account-exists-with-different-credential':
                fbcred = err.credential;
                this.fetchProvidersForEmail(err.email).then((methods) => {
                  if (methods[0] === 'google.com') {
                    if (this.platform == 'ios') {
                      let _err = err;
                      _err.method = 'google.com';
                      _err.message = 'Email ' + err.email + ' has been used with Google login. Do you want to continue with Google login?';
                      reject(_err);
                    } else {
                      var provider = new firebase.auth.GoogleAuthProvider();
                      provider.setCustomParameters({ 'login_hint': err.email });
                      firebase.auth().signInWithRedirect(provider).then((res) => {
                        firebase.auth().currentUser.linkAndRetrieveDataWithCredential(fbcred);
                      }).then((res: any) => {
                          //set user token
                          this.storage.set(TOKEN_KEY, 'Bearer' + res.uid).then(() => {
                            this.authenticationState.next(true);
                          });
                          resolve(res);
                      }).catch((err) => {
                        reject(err);
                      });
                    }
                  } else {
                    let _err = err;
                    _err.method = 'password';
                    _err.message = 'Email ' + err.email + ' has been used, do you want to continue login with this email?';
                    reject(_err);
                  }
                }).catch((err) => {
                  reject(err);
                });
                break;

              default:
                reject(err);
                break;
            }
          });

        }).catch(err => {
          reject(err);
        });
      }
    });
  }

  fetchProvidersForEmail(email: string) {
    return new Promise((resolve, reject) => {
      firebase.auth().fetchSignInMethodsForEmail(email).then((res) => {
        console.log(res);
        if (res.length > 0) resolve(res);
        else reject(true);
      });
    });
  }

  genSocialUser(res) {
    return {
      avatar: res.photoURL || null,
      phone: res.phoneNumber || null,
      uid: res.uid,
      name: res.displayName,
      email: res.email,
      emailVerified: res.emailVerified,
    }
  }

  sendEmailConfirm() {
    firebase.auth().currentUser.sendEmailVerification().then(function () { }, function (error) { });
  }

  createUser(uid, user) {
    return new Promise((resolve, reject) => {
      user.uid = uid;
      this.user.createUser(uid, user).then(() => {
        this.user.setUserGlobalVariable(uid).then((res: any) => {
          this.sendEmailConfirm();
          resolve(true);
        });
      }, (err) => {
        reject(false);
      });
    });
  }

 
  signOut() {
    this.afAuth.auth.signOut();
    if (this.platform.is('cordova')) {
      this.gplus.logout();
    }
  }
}

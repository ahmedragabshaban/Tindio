import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController ,NavController } from '@ionic/angular';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  load: any;
  constructor(private loader: LoadingController, private nav:NavController,public alert: AlertController, private auth: AuthService) {
    this.nav.navigateRoot('home')
   }

  ngOnInit() {
  }
loginGoogle() {
    this.present().then(() => {
      this.auth.loginGoogle().then((res) => {
        console.log(res);
     //just for testing
     this.auth.authenticationState.next(true);

        if (this.auth.basic.phone) {
          this.dismiss();
              this.nav.navigateRoot('home')
        } else {
          if (this.auth.basic) {
            this.dismiss();
            // this.nav.setRoot('PhoneAuthPage', {action: 'verif', user: this.auth.basic});
          }
        }
      }, err => {
        console.log(err);
        //just for testing
        this.auth.authenticationState.next(true);

        this.dismiss();
        switch (err.code) {
          case 'auth/account-exists-with-different-credential':
            this.showConfirm(err);
            break;
          default:
            break;
        }
      });
    });
  }



  loginFB() {
      this.present().then(() => {
        this.auth.loginFb().then((res) => {
          console.log(res);
     //just for testing
     this.auth.authenticationState.next(true);

          if(this.auth.basic.phone){
            this.dismiss();
                this.nav.navigateRoot('home')
          }else{
            if(this.auth.basic) {
              this.dismiss();
            }
          }
        }, err =>{
          console.log(err);
               //just for testing
        this.auth.authenticationState.next(true);

          this.dismiss();
          switch (err.code) {
            case 'auth/account-exists-with-different-credential':
              this.showConfirm(err);
              break;
            default:
              break;
          }
        });
      });
  }
  async showConfirm(param) {
    const alert = await this.alert.create({
      header: 'Email Already Exist',
      message: param.message,
      buttons: [{
        text: 'No',
        handler: () => {
        }
      }, {
        text: 'YES',
        handler: () => {
          if (param.method === 'facebook.com')
            this.loginFB();
          if (param.method === 'google.com')
            this.loginGoogle();
          // if(param.method === 'password')
          //    this.nav.push('LoginEmailPage');
        }
      }
      ]
    })
    await alert.present();
  }


  async present() {
    this.load = await this.loader.create({
      message: `
      <div class="loading-custom-spinner-container">
        <div class="loading-custom-spinner-box"></div>
      </div>
      <div>Please wait ...</div>`,
      spinner: 'crescent',
    });
    return await this.load.present();
  }


  dismiss() {
    this.load.dismiss();
  }

}

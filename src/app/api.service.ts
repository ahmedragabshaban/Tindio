// best practice to separate presentation of data from data access by encapsulating data access in a separate service
// and delegating to that service in the component, even in simple cases like this task 
// to made data access logic be re-used or standardized.

import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  Url = 'http://tindiostag.tindio.com/api/';
  corsHeaders = {
    headers:new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  constructor(private http: HttpClient)   { }
//we could use a public get function with url param ,but for this request we may use it a lot i will made it as a function
   
  getProducts(page:number) {
    const url=this.Url+`home?items_per_page=`+24+`&page=`+page
    return this.http.get(url,this.corsHeaders)
  }

}

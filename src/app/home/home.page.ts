import { Component } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  products: any;
  counter = 0
  constructor(private api: ApiService) {
    this.products = []
    this.loadData(null)
  }

  loadData(event) {
    this.api.getProducts(this.counter).subscribe((res): any => {
      const temp: any = res;
      if (temp.list.length !== 0) {
        //append data
        this.products = this.products.concat(temp.list);
        // to get next page
        this.counter++
      }
      else {
        // disable the infinite scroll
        event.target.disabled = true;
      }
      setTimeout(() => {
        //  disable scroll loading
        if (event) { event.target.complete() }
      }, 500);
    });
  }

}
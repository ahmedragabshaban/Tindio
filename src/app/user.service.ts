import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  database: string = "user";
  user: any = {
    isLogin: false
  };

  constructor() {}

  get timestamp() {
      return firebase.firestore.FieldValue.serverTimestamp()
  }

  createUser(uid, data: any){
    data.createdAt = this.timestamp;
    delete data.pass; delete data.pass2;
    return this.ref.firestore.doc(this.database+'/'+uid).set(data);
  }

  setUserGlobalVariable(uid){    
    return new Promise((resolve)=>{
      
    });
  }

  get getDataUser(){
    if(this.user.uid){
  
    }
    return null;
  }

  get isLogin(){
    return this.user.isLogin;
  }

  setLogout(){
    this.user = {};
  }

  get basic(){
    return this.user || null;
  }
  
  get ref(){
    return firebase.firestore().collection(this.database);
  }

  getUserby(by, data){
    return new Promise((resolve, reject)=>{
      firebase.firestore().collection(this.database).where(by, '==', data).get().then(snap=>{
        if(snap.size > 0){
          snap.forEach(data=>{
            resolve(data.data());
          });
        }
        else reject(false);
      });
    });
  }

  create(key, data){
      data.uid = key;
      data.timestamp = new Date().getTime();
  }

  update(data){
  }

  delete(data){
  }
}

